USE [TravelManagement]
GO
/****** Object:  StoredProcedure [dbo].[AddBedType]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AddBedType]
@type_name nvarchar(100)

as

insert into bed_type (type) values (@type_name)


GO
/****** Object:  StoredProcedure [dbo].[AddNewRoomPackage]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[AddNewRoomPackage]
@travel_package_id int,
@travel_start date,
@travel_end date,
@room_type int,
@bed_type int,
@price_adult float,
@price_child float,
@price_toddler float

As

Insert into room_package(travel_package_id, travel_sdate, travel_edate, room_type, bed_type, price_adult, price_child, price_toddler)
values(@travel_package_id, @travel_start, @travel_end, @room_type, @bed_type, @price_adult, @price_child, @price_toddler)
select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[AddNewTravelPackage]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[AddNewTravelPackage]
@name nvarchar(255),
@book_start date,
@book_end date,
@min_pax int,
@min_night int,
@max_night int

As

Insert into travel_package(package_name, booking_sdate, booking_edate, 
min_pax, min_night, max_night)
VALUES (@name, @book_start, @book_end, @min_pax, @min_night, @max_night)
Select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[AddRoomType]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AddRoomType]
@type_name nvarchar(100)

as

insert into room_type (type) values (@type_name)


GO
/****** Object:  StoredProcedure [dbo].[DeleteRoomPackageById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[DeleteRoomPackageById]
@rp_id int

as

delete from room_package
where room_package_id = @rp_id
GO
/****** Object:  StoredProcedure [dbo].[DeleteTravelPackageById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[DeleteTravelPackageById]
@id int

As

Delete from travel_package
where id = @id
GO
/****** Object:  StoredProcedure [dbo].[RetrieveAllBedTypes]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveAllBedTypes]
AS
Select * from bed_type
GO
/****** Object:  StoredProcedure [dbo].[RetrieveAllRoomTypes]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveAllRoomTypes]
AS
Select * from room_type
GO
/****** Object:  StoredProcedure [dbo].[RetrieveAllTravelPackages]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveAllTravelPackages]
As

Select * from travel_package
GO
/****** Object:  StoredProcedure [dbo].[RetrieveBedTypeById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[RetrieveBedTypeById]
@bed_id int

As

Select * from bed_type where bed_id = @bed_id

GO
/****** Object:  StoredProcedure [dbo].[RetrieveExtensionByRoomPackageId]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveExtensionByRoomPackageId]
@rp_id int

As

select * from extension where room_package_id = @rp_id;
GO
/****** Object:  StoredProcedure [dbo].[RetrieveRoomPackageById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveRoomPackageById]
@rp_id int

As

Select * from room_package where room_package_id = @rp_id
GO
/****** Object:  StoredProcedure [dbo].[RetrieveRoomPackagesByTravelPackageId]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveRoomPackagesByTravelPackageId]
@tp_id int

as

select * from room_package where travel_package_id = @tp_id
GO
/****** Object:  StoredProcedure [dbo].[RetrieveRoomTypeById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[RetrieveRoomTypeById]
@room_id int

As

Select * from room_type where room_id = @room_id

GO
/****** Object:  StoredProcedure [dbo].[RetrieveTravelPackageById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveTravelPackageById]
@id int

AS

Select * from travel_package
where id = @id
GO
/****** Object:  StoredProcedure [dbo].[UpdateRoomPackageById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[UpdateRoomPackageById]
@rp_id int,
@tp_id int,
@travel_start date,
@travel_end date,
@room_type int,
@bed_type int,
@price_adult float,
@price_child float,
@price_toddler float

as

update room_package
set travel_package_id = @tp_id,
travel_sdate = @travel_start,
travel_edate = @travel_end,
room_type = @room_type,
bed_type = @bed_type,
price_adult = @price_adult,
price_child = @price_child,
price_toddler = @price_toddler
where room_package_id = @rp_id
GO
/****** Object:  StoredProcedure [dbo].[UpdateTravelPackageById]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[UpdateTravelPackageById]
@id int,
@name nvarchar(255),
@book_start date,
@book_end date,
@min_pax int,
@min_night int,
@max_night int

As

Update travel_package
set 
package_name = @name,
booking_sdate = @book_start,
booking_edate = @book_end,
min_pax = @min_pax,
min_night = @min_night,
max_night = @max_night
where id = @id


GO
/****** Object:  Table [dbo].[bed_type]    Script Date: 26/9/2014 12:25:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bed_type](
	[bed_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_bed_type] PRIMARY KEY CLUSTERED 
(
	[bed_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[extension]    Script Date: 26/9/2014 12:25:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[extension](
	[room_package_id] [int] NOT NULL,
	[price_adult] [float] NOT NULL,
	[price_child] [float] NOT NULL,
	[price_toddler] [float] NOT NULL,
 CONSTRAINT [PK_extension] PRIMARY KEY CLUSTERED 
(
	[room_package_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[room_package]    Script Date: 26/9/2014 12:25:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[room_package](
	[room_package_id] [int] IDENTITY(1,1) NOT NULL,
	[travel_package_id] [int] NOT NULL,
	[travel_sdate] [date] NOT NULL,
	[travel_edate] [date] NOT NULL,
	[room_type] [int] NOT NULL,
	[bed_type] [int] NOT NULL,
	[price_adult] [float] NOT NULL,
	[price_child] [float] NOT NULL,
	[price_toddler] [float] NOT NULL,
 CONSTRAINT [PK_room_package] PRIMARY KEY CLUSTERED 
(
	[room_package_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[room_type]    Script Date: 26/9/2014 12:25:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[room_type](
	[room_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_room_type] PRIMARY KEY CLUSTERED 
(
	[room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[travel_package]    Script Date: 26/9/2014 12:25:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[travel_package](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[package_name] [nvarchar](255) NOT NULL,
	[booking_sdate] [date] NOT NULL,
	[booking_edate] [date] NOT NULL,
	[min_pax] [int] NOT NULL,
	[min_night] [int] NOT NULL,
	[max_night] [int] NOT NULL,
 CONSTRAINT [PK_travel_package] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[bed_type] ON 

INSERT [dbo].[bed_type] ([bed_id], [type]) VALUES (1, N'Twin Sharing')
INSERT [dbo].[bed_type] ([bed_id], [type]) VALUES (2, N'Single')
INSERT [dbo].[bed_type] ([bed_id], [type]) VALUES (3, N'Double')
SET IDENTITY_INSERT [dbo].[bed_type] OFF
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (2, 183, 110, 0)
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (3, 321, 193, 0)
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (4, 183, 110, 0)
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (5, 202, 121, 0)
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (6, 353, 212, 0)
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (7, 202, 121, 0)
INSERT [dbo].[extension] ([room_package_id], [price_adult], [price_child], [price_toddler]) VALUES (8, 238, 143, 0)
SET IDENTITY_INSERT [dbo].[room_package] ON 

INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (2, 2, CAST(0x31390B00 AS Date), CAST(0x68390B00 AS Date), 1, 1, 1608, 1140, 810)
INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (3, 2, CAST(0x69390B00 AS Date), CAST(0x6F390B00 AS Date), 1, 1, 2022, 1388, 810)
INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (4, 2, CAST(0x51390B00 AS Date), CAST(0xBC390B00 AS Date), 1, 1, 1608, 1140, 810)
INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (5, 2, CAST(0x31390B00 AS Date), CAST(0x68390B00 AS Date), 2, 1, 1665, 1173, 810)
INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (6, 2, CAST(0x69390B00 AS Date), CAST(0x6F390B00 AS Date), 2, 1, 2118, 1446, 810)
INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (7, 2, CAST(0x70390B00 AS Date), CAST(0xBC390B00 AS Date), 2, 1, 1665, 1773, 810)
INSERT [dbo].[room_package] ([room_package_id], [travel_package_id], [travel_sdate], [travel_edate], [room_type], [bed_type], [price_adult], [price_child], [price_toddler]) VALUES (8, 2, CAST(0x31390B00 AS Date), CAST(0x54390B00 AS Date), 3, 1, 1774, 1238, 810)
SET IDENTITY_INSERT [dbo].[room_package] OFF
SET IDENTITY_INSERT [dbo].[room_type] ON 

INSERT [dbo].[room_type] ([room_id], [type]) VALUES (1, N'Superior')
INSERT [dbo].[room_type] ([room_id], [type]) VALUES (2, N'Deluxe Garden')
INSERT [dbo].[room_type] ([room_id], [type]) VALUES (3, N'Deluxe Seaview')
INSERT [dbo].[room_type] ([room_id], [type]) VALUES (4, N'Suite')
SET IDENTITY_INSERT [dbo].[room_type] OFF
SET IDENTITY_INSERT [dbo].[travel_package] ON 

INSERT [dbo].[travel_package] ([id], [package_name], [booking_sdate], [booking_edate], [min_pax], [min_night], [max_night]) VALUES (2, N'Club Med Kabira, Japan', CAST(0x0C390B00 AS Date), CAST(0x30390B00 AS Date), 2, 3, 7)
INSERT [dbo].[travel_package] ([id], [package_name], [booking_sdate], [booking_edate], [min_pax], [min_night], [max_night]) VALUES (3, N'Ocean Park, Hong Kong', CAST(0x30390B00 AS Date), CAST(0x53390B00 AS Date), 2, 3, 7)
SET IDENTITY_INSERT [dbo].[travel_package] OFF
ALTER TABLE [dbo].[extension]  WITH CHECK ADD  CONSTRAINT [FK_extension_room_package] FOREIGN KEY([room_package_id])
REFERENCES [dbo].[room_package] ([room_package_id])
GO
ALTER TABLE [dbo].[extension] CHECK CONSTRAINT [FK_extension_room_package]
GO
ALTER TABLE [dbo].[room_package]  WITH CHECK ADD  CONSTRAINT [FK_room_package_bed_type] FOREIGN KEY([bed_type])
REFERENCES [dbo].[bed_type] ([bed_id])
GO
ALTER TABLE [dbo].[room_package] CHECK CONSTRAINT [FK_room_package_bed_type]
GO
ALTER TABLE [dbo].[room_package]  WITH CHECK ADD  CONSTRAINT [FK_room_package_room_type] FOREIGN KEY([room_type])
REFERENCES [dbo].[room_type] ([room_id])
GO
ALTER TABLE [dbo].[room_package] CHECK CONSTRAINT [FK_room_package_room_type]
GO
ALTER TABLE [dbo].[room_package]  WITH CHECK ADD  CONSTRAINT [FK_room_package_travel_package] FOREIGN KEY([travel_package_id])
REFERENCES [dbo].[travel_package] ([id])
GO
ALTER TABLE [dbo].[room_package] CHECK CONSTRAINT [FK_room_package_travel_package]
GO
