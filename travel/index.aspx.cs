﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using travel.lib;

namespace travel
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadTravelPackages();
            }
        }

        private void LoadTravelPackages()
        {
            //init datatable
            DataTable tableToBind = new DataTable();
            tableToBind.Columns.Add("travel_package_id");
            tableToBind.Columns.Add("package_name");
            tableToBind.Columns.Add("start_date");
            tableToBind.Columns.Add("end_date");
            tableToBind.Columns.Add("min_pax");
            tableToBind.Columns.Add("min_night");
            tableToBind.Columns.Add("max_night");

            //get data to load into gridview
            SqlCommand comm = new SqlCommand("RetrieveAllTravelPackages");
            DataTable allTPackages = DB.SPExecuteSelectStatements(comm);


            foreach (DataRow packagesRow in allTPackages.Rows)
            {
                string id = packagesRow["id"].ToString();
                string name = packagesRow["package_name"].ToString();
                string book_start = packagesRow["booking_sdate"].ToString();
                string book_end = packagesRow["booking_edate"].ToString();
                string min_pax = packagesRow["min_pax"].ToString();
                string min_night = packagesRow["min_night"].ToString();
                string max_night = packagesRow["max_night"].ToString();

                //now we are done lets add it to the row of the datatable
                tableToBind.Rows.Add(id, name, book_start, book_end, min_pax, min_night, max_night);
            }

            //now after iterating through all, lets bind it to the gridview
            //hide the ID column
            gvTravelPackages.Columns[0].Visible = false;
            gvTravelPackages.DataSource = tableToBind;
            gvTravelPackages.DataBind();
        }

        protected void gvTravelPackages_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvTravelPackages.DataKeys[e.RowIndex].Value.ToString());
            System.Diagnostics.Debug.Print(id + "");

            SqlCommand comm = new SqlCommand("DeleteTravelPackageById");
            comm.Parameters.AddWithValue("@id", id);

            DB.SPExecuteNonQuery(comm);

            LoadTravelPackages();

        }

        protected void gvTravelPackages_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditTravelPackage")
            {
                if (Session["editTravelPackageID"] != null)
                {
                    Session["editTravelPackageID"] = e.CommandArgument;
                }
                else
                {
                    Session.Add("editTravelPackageID", e.CommandArgument);
                }

                //commented due to limited time.
                //Response.Redirect("~/EditTravelPackage.aspx");
            }
        }
    }
    
}