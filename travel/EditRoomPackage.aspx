﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app.Master" AutoEventWireup="true" CodeBehind="EditRoomPackage.aspx.cs" Inherits="travel.EditRoomPackage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <h3>Edit Air Package</h3>
    </div>
    <div class="container-fluid">
        <div role="form">
            <div class="form-group">
                <label for="ddlTP">Travel Package</label>
                <asp:DropDownList ID="ddlTP" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="form-group">
                <label for="travel_start">Start Date</label>
                <asp:TextBox ID="travel_start" runat="server" type="date" class="form-control" placeholder="Start Date" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="travel_end">End Date</label>
                <asp:TextBox ID="travel_end" runat="server" type="date" class="form-control" placeholder="End Date" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="ddlRoom">Room Type</label>
                <asp:DropDownList ID="ddlRoom" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="form-group">
                <label for="ddlBed">Bed Type</label>
                <asp:DropDownList ID="ddlBed" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="form-group">
                <label for="price_adult">Price Per Adult</label>
                <asp:TextBox ID="price_adult" runat="server" type="number" step="any" min="0" class="form-control" placeholder="Price Per Adult" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="price_child">Price Per Child 4 - 11 yrs</label>
                <asp:TextBox  ID="price_child" runat="server" type="number" step="any" min="0" class="form-control" placeholder="Price Per Child 4 - 11 yrs" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="price_toddler">Price Per Child 0 - 3 yrs</label>
                <asp:TextBox  ID="price_toddler" runat="server" type="number" step="any" min="0" class="form-control" placeholder="Price Per Child 0 - 3 yrs" required></asp:TextBox>
            </div>
    </div>
        <asp:Button ID="btnUpdate" runat="server" Text="Update Package" OnClick="btnCreate_Click" ValidationGroup="create" CssClass="btn btn-default" />
        <p class="bg-success" runat="server" id="success" visible="false">Package Edited Successfully.</p>
        <p class="bg-danger" runat="server" id="error" visible="false">Error Editing Package. Please check your inputs.</p>
    </div>
</asp:Content>
