﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace travel.lib
{
    public class DB
    {
        public static int ExecuteNonQuery(SqlCommand command)
        {

            int rowsAffected = 0;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMSDBConnection"].ConnectionString);
            command.Connection = conn;
            conn.Open();
            rowsAffected = command.ExecuteNonQuery();
            conn.Close();

            return rowsAffected;

        }

        //execute a scalar query type (return only one row) of sql query, and return
        //the first and only row
        public static string ExecuteScalarCommand(SqlCommand command)
        {

            string dataReturned = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMSDBConnection"].ConnectionString);
            command.Connection = conn;
            try
            {
                conn.Open();
            }
            catch (SqlException e)
            {

            }


            object value = command.ExecuteScalar();

            if (value != null)
            {
                dataReturned = value.ToString();
            }
            conn.Close();
            return dataReturned;

        }
        //execute a select sql query, and return a table with the data in it
        public static DataTable ExecuteSelectStatements(SqlCommand command)
        {

            DataTable table = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMSDBConnection"].ConnectionString);
            command.Connection = conn;
            try
            {
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                table.Load(reader);
            }
            catch (Exception e)
            {

            }
            finally
            {
                conn.Close();
            }

            return table;

        }

        //execute a Stored Procedure non query type of sql query (create, update, delete), and return
        //no.of rows affected
        public static int SPExecuteNonQuery(SqlCommand command)
        {

            int rowsAffected = 0;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMSDBConnection"].ConnectionString);
            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;
            conn.Open();
            rowsAffected = command.ExecuteNonQuery();
            conn.Close();

            return rowsAffected;

        }

        //execute a Stored Procedure scalar query type (return only one row) of sql query, and return
        //the first and only row
        public static string SPExecuteScalarCommand(SqlCommand command)
        {

            string dataReturned = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMSDBConnection"].ConnectionString);
            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;
            conn.Open();
            object value = command.ExecuteScalar();

            if (value != null)
            {
                dataReturned = value.ToString();
            }
            conn.Close();
            return dataReturned;


        }
        //execute a Stored Procedure select sql query, and return a table with the data in it
        public static DataTable SPExecuteSelectStatements(SqlCommand command)
        {

            DataTable table = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMSDBConnection"].ConnectionString);
            command.Connection = conn;
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                table.Load(reader);
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }

            return table;

        }

    }
}