﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using travel.lib;

namespace travel
{
    public partial class EditRoomPackage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadTravelPackages();
                LoadRoomTypes();
                LoadBedTypes();
                LoadPackageDetails();
            }
        }

        /**
         *  load data of room/air package to be edited 
         **/
        private void LoadPackageDetails()
        {
            if (Session["roomPackageID"] != null)
            {
                int rp_id = Convert.ToInt32(Session["roomPackageID"]);

                SqlCommand comm = new SqlCommand("RetrieveRoomPackageById");
                comm.Parameters.AddWithValue("@rp_id", rp_id);
                DataTable rPackage = DB.SPExecuteSelectStatements(comm);

                if (rPackage.Rows.Count > 0)
                {
                    string id = rPackage.Rows[0]["room_package_id"].ToString();
                    int tp_id = Convert.ToInt32(rPackage.Rows[0]["travel_package_id"]);
                    DateTime t_start = Convert.ToDateTime(rPackage.Rows[0]["travel_sdate"]);
                    DateTime t_end = Convert.ToDateTime(rPackage.Rows[0]["travel_edate"]);
                    int room_type = Convert.ToInt32(rPackage.Rows[0]["room_type"]);
                    int bed_type = Convert.ToInt32(rPackage.Rows[0]["bed_type"]);
                    string price_adult_p = rPackage.Rows[0]["price_adult"].ToString();
                    string price_child_p = rPackage.Rows[0]["price_child"].ToString();
                    string price_toddler_p = rPackage.Rows[0]["price_toddler"].ToString();


                    ddlTP.SelectedValue = tp_id.ToString();
                    ddlRoom.SelectedValue = room_type.ToString();
                    ddlBed.SelectedValue = bed_type.ToString();
                    travel_start.Text = t_start.ToString("yyyy-MM-dd");
                    travel_end.Text = t_end.ToString("yyyy-MM-dd");
                    price_adult.Text = price_adult_p;
                    price_child.Text = price_child_p;
                    price_toddler.Text = price_toddler_p;
                }

            }
        }

        /**
         * load the existing list of room type:i.e. superior, double etc etc
         * */
        private void LoadRoomTypes()
        {
            //call and execute stored procedure
            SqlCommand comm = new SqlCommand("RetrieveAllRoomTypes");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            DataView view = packagesTable.AsDataView();
            view.Sort = "type asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                ListItem item = new ListItem(packagesRow["type"].ToString(), packagesRow["room_id"].ToString());
                ddlRoom.Items.Add(item);
            }
        }

        /**
         *  load existing list of bed types
         **/
        private void LoadBedTypes()
        {
            //call and execute stored procedure
            SqlCommand comm = new SqlCommand("RetrieveAllBedTypes");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            DataView view = packagesTable.AsDataView();
            view.Sort = "type asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                ListItem item = new ListItem(packagesRow["type"].ToString(), packagesRow["bed_id"].ToString());
                ddlBed.Items.Add(item);
            }
        }

        /*
         * load existing list of travel packages
         **/
        private void LoadTravelPackages()
        {
            //call and execute store procedure
            SqlCommand comm = new SqlCommand("RetrieveAllTravelPackages");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            DataView view = packagesTable.AsDataView();
            view.Sort = "package_name asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                ListItem item = new ListItem(packagesRow["package_name"].ToString(), packagesRow["id"].ToString());
                ddlTP.Items.Add(item);
            }
        }


        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Session["roomPackageID"] != null)
            {
                success.Visible = false;
                error.Visible = false;

                DateTime sdate = Convert.ToDateTime(travel_start.Text);
                DateTime edate = Convert.ToDateTime(travel_end.Text);
                int time = DateTime.Compare(sdate, edate);

                //check if start date is greater than end date.
                if (time >= 0)
                {
                    error.Visible = true;
                    return;
                }

                int rpId = Convert.ToInt32(Session["roomPackageID"]);
                int tpId = Convert.ToInt32(ddlTP.SelectedValue);
                int roomId = Convert.ToInt32(ddlRoom.SelectedValue);
                int bedId = Convert.ToInt32(ddlBed.SelectedValue);
                double priceA = Convert.ToDouble(price_adult.Text);
                double priceC = Convert.ToDouble(price_child.Text);
                double priceT = Convert.ToDouble(price_toddler.Text);

                SqlCommand comm = new SqlCommand("UpdateRoomPackageById");

                SqlParameter param = new SqlParameter("@rp_id", rpId);
                comm.Parameters.Add(param);

                param = new SqlParameter("@tp_id", tpId);
                comm.Parameters.Add(param);

                param = new SqlParameter("@travel_start", sdate);
                comm.Parameters.Add(param);

                param = new SqlParameter("@travel_end", edate);
                comm.Parameters.Add(param);

                param = new SqlParameter("@room_type", roomId);
                comm.Parameters.Add(param);

                param = new SqlParameter("@bed_type", bedId);
                comm.Parameters.Add(param);

                param = new SqlParameter("@price_adult", priceA);
                comm.Parameters.Add(param);

                param = new SqlParameter("@price_child", priceC);
                comm.Parameters.Add(param);

                param = new SqlParameter("@price_toddler", priceT);
                comm.Parameters.Add(param);

                //execute stored procedure
                int packageId = Convert.ToInt32(DB.SPExecuteNonQuery(comm));

                if (packageId > 0)
                {
                    success.Visible = true;
                    Response.Redirect("~/viewRoomPackages.aspx");
                }
                else
                {
                    error.Visible = true;
                }
            }
        }

        protected void ClearAllFields()
        {
            ddlTP.SelectedIndex = 0;
            ddlRoom.SelectedIndex = 0;
            ddlBed.SelectedIndex = 0;
            travel_start.Text = "";
            travel_end.Text = "";
            price_adult.Text = "";
            price_child.Text = "";
            price_toddler.Text = "";
        }
    }
}