﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using travel.lib;

namespace travel
{
    public partial class CreateRoomPackage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadTravelPackages();
            LoadRoomTypes();
            LoadBedTypes();
        }

        private void LoadRoomTypes()
        {
            SqlCommand comm = new SqlCommand("RetrieveAllRoomTypes");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            DataView view = packagesTable.AsDataView();
            //sort the view
            view.Sort = "type asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                //add items to dropdown
                ListItem item = new ListItem(packagesRow["type"].ToString(), packagesRow["room_id"].ToString());
                ddlRoom.Items.Add(item);
            }
        }

        private void LoadBedTypes()
        {
            SqlCommand comm = new SqlCommand("RetrieveAllBedTypes");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            DataView view = packagesTable.AsDataView();
            //sort view
            view.Sort = "type asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                ListItem item = new ListItem(packagesRow["type"].ToString(), packagesRow["bed_id"].ToString());
                ddlBed.Items.Add(item);
            }
        }

        private void LoadTravelPackages()
        {
            SqlCommand comm = new SqlCommand("RetrieveAllTravelPackages");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            //sort view
            DataView view = packagesTable.AsDataView();
            view.Sort = "package_name asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                ListItem item = new ListItem(packagesRow["package_name"].ToString(), packagesRow["id"].ToString());
                ddlTP.Items.Add(item);
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            success.Visible = false;
            error.Visible = false;
            DateTime sdate = Convert.ToDateTime(travel_start.Text);
            DateTime edate = Convert.ToDateTime(travel_end.Text);
            int time = DateTime.Compare(sdate, edate);

            //check if start date greater than end date
            if(time >= 0)
            {
                error.Visible = true;
                return;
            }

            //get all params
            int tpId = Convert.ToInt32(ddlTP.SelectedValue);
            int roomId = Convert.ToInt32(ddlRoom.SelectedValue);
            int bedId = Convert.ToInt32(ddlBed.SelectedValue);
            double priceA = Convert.ToDouble(price_adult.Text);
            double priceC = Convert.ToDouble(price_child.Text);
            double priceT = Convert.ToDouble(price_toddler.Text);

            //call stored procedure
            SqlCommand comm = new SqlCommand("AddNewRoomPackage");

            //add in all params
            SqlParameter param = new SqlParameter("@travel_package_id", tpId);
            comm.Parameters.Add(param);

            param = new SqlParameter("@travel_start", sdate);
            comm.Parameters.Add(param);

            param = new SqlParameter("@travel_end", edate);
            comm.Parameters.Add(param);

            param = new SqlParameter("@room_type", roomId);
            comm.Parameters.Add(param);

            param = new SqlParameter("@bed_type", bedId);
            comm.Parameters.Add(param);

            param = new SqlParameter("@price_adult", priceA);
            comm.Parameters.Add(param);

            param = new SqlParameter("@price_child", priceC);
            comm.Parameters.Add(param);

            param = new SqlParameter("@price_toddler", priceT);
            comm.Parameters.Add(param);

            //execute procedure
            int packageId = Convert.ToInt32(DB.SPExecuteScalarCommand(comm));

            if(packageId > 0)
            {
                success.Visible = true;
                ClearAllFields();
            }
            else
            {
                error.Visible = true;
            }
        }

        protected void ClearAllFields()
        {
            ddlTP.SelectedIndex = 0;
            ddlRoom.SelectedIndex = 0;
            ddlBed.SelectedIndex = 0;
            travel_start.Text = "";
            travel_end.Text = "";
            price_adult.Text = "";
            price_child.Text = "";
            price_toddler.Text = "";
        }
    }
}