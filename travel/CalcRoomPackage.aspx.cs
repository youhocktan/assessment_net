﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using travel.lib;

namespace travel
{
    public partial class CalcRoomPackage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadPackageDetails();
            }
        }

        private void LoadPackageDetails()
        {
            if (Session["roomPackageID"] != null)
            {
                int rp_id = Convert.ToInt32(Session["roomPackageID"]);

                //call and execute stored procedure
                SqlCommand comm = new SqlCommand("RetrieveRoomPackageById");
                comm.Parameters.AddWithValue("@rp_id", rp_id);
                DataTable rPackage = DB.SPExecuteSelectStatements(comm);

                //check if package exist
                if (rPackage.Rows.Count > 0)
                {
                    string id = rPackage.Rows[0]["room_package_id"].ToString();
                    int tp_id = Convert.ToInt32(rPackage.Rows[0]["travel_package_id"]);
                    DateTime t_start = Convert.ToDateTime(rPackage.Rows[0]["travel_sdate"]);
                    DateTime t_end =  Convert.ToDateTime(rPackage.Rows[0]["travel_edate"]);
                    int room_type = Convert.ToInt32(rPackage.Rows[0]["room_type"]);
                    int bed_type = Convert.ToInt32(rPackage.Rows[0]["bed_type"]);
                    string price_adult = rPackage.Rows[0]["price_adult"].ToString();
                    string price_child = rPackage.Rows[0]["price_child"].ToString();
                    string price_toddler = rPackage.Rows[0]["price_toddler"].ToString();

                    comm = new SqlCommand("RetrieveTravelPackageById");
                    comm.Parameters.AddWithValue("@id", tp_id);
                    DataTable tp = DB.SPExecuteSelectStatements(comm);
                    string tp_name = tp.Rows[0]["package_name"].ToString();

                    comm = new SqlCommand("RetrieveRoomTypeById");
                    comm.Parameters.AddWithValue("@room_id", room_type);
                    DataTable room = DB.SPExecuteSelectStatements(comm);
                    string room_name = room.Rows[0]["type"].ToString();

                    comm = new SqlCommand("RetrieveBedTypeById");
                    comm.Parameters.AddWithValue("@bed_id", bed_type);
                    DataTable bed = DB.SPExecuteSelectStatements(comm);
                    string bed_name = bed.Rows[0]["type"].ToString();

                    comm = new SqlCommand("RetrieveExtensionByRoomPackageId");
                    comm.Parameters.AddWithValue("@rp_id", rp_id);
                    DataTable extension = DB.SPExecuteSelectStatements(comm);
                    string extension_adult = extension.Rows[0]["price_adult"].ToString();
                    string extension_child = extension.Rows[0]["price_child"].ToString();
                    string extension_toddler = extension.Rows[0]["price_toddler"].ToString();

                    travel_package_name.Text = tp_name;
                    package_date.Text = t_start.ToString("dd/MM/yyyy") + " - " + t_end.ToString("dd/MM/yyyy");
                    r_type.Text = room_name;
                    b_type.Text = bed_name;
                    price_adult_p.Text = price_adult;
                    price_child_p.Text = price_child;
                    price_toddler_p.Text = price_toddler;
                    price_adult_e.Text = extension_adult;
                    price_child_e.Text = extension_child;
                    price_toddler_e.Text = extension_toddler;

                    travel_start.Attributes.Add("min", t_start.ToString("yyyy-MM-dd"));
                    travel_start.Attributes.Add("max", (t_end).AddDays(-4).ToString("yyyy-MM-dd"));
                    travel_end.Attributes.Add("min", t_start.ToString("yyyy-MM-dd"));
                    travel_end.Attributes.Add("max", t_end.ToString("yyyy-MM-dd"));

                }

            }
        }


        protected void btnCalc_Click(object sender, EventArgs e)
        {
            error.Visible = false;
            DateTime sdate = Convert.ToDateTime(travel_start.Text);
            DateTime edate = Convert.ToDateTime(travel_end.Text);

            //check if start date is greater than end date
            int time = DateTime.Compare(sdate, edate);
            if (time >= 0)
            {
                error.InnerText = "Start date must be smaller than end date.";
                error.Visible = true;
                return;
            }

            //if start date and end date is ok, get days difference
            int daysDiff = ((TimeSpan) (edate - sdate)).Days;

            //check max number of nights
            if(daysDiff > 7)
            {
                error.InnerText = "Maximum number of nights is 7.";
                error.Visible = true;
                return;
            }

            //check min number of nights
            if(daysDiff < 3)
            {
                error.InnerText = "Minimum number of nights is 3.";
                error.Visible = true;
                return;
            }

            //get all number values to do calculation
            double pa = Convert.ToDouble(price_adult_p.Text);
            double pc = Convert.ToDouble(price_child_p.Text);
            double pt = Convert.ToDouble(price_toddler_p.Text);
            double ea = Convert.ToDouble(price_adult_e.Text);
            double ec = Convert.ToDouble(price_child_e.Text);
            double et = Convert.ToDouble(price_toddler_e.Text);
            int noAdult = Convert.ToInt32(numAdult.Text);
            int noChild = Convert.ToInt32(numChild.Text);
            int noToddler = Convert.ToInt32(numToddler.Text);

            double total = 0;
            string breakdown = "";
            
            //take package price
            total = noAdult * pa + noChild * pc + noToddler * pt;
            breakdown = "<p>Package:</p>"
                + "<p>Number of Adult:" + noAdult + " x " + pa + "</p>"
                + "<p>Number of Children (4-11 yrs):" + noChild + " x " + pc + "</p>"
                + "<p>Number of Children (0-3 yrs):" + noToddler + " x " + pt + "</p>";

            //take extension price
            if(daysDiff - 3 > 0)
            {
                total += (daysDiff - 3) * (noAdult * ea + noChild * ec + noToddler * et);
                breakdown += "<p>Extension:</p>"
                + "<p>Number of Adult:" + noAdult + " x " + ea + " x " + (daysDiff - 3) +" nights</p>"
                + "<p>Number of Children (4-11 yrs):" + noChild + " x " + ec + " x " + (daysDiff - 3) + " nights</p>"
                + "<p>Number of Children (0-3 yrs):" + noToddler + " x " + et + " x " + (daysDiff - 3) + " nights</p>";
            }

            breakdown += "<p>Total: $" + total + "</p>";

            //set html to code.
            breakdown_amount.InnerHtml = breakdown;
            System.Diagnostics.Debug.Print(total + "");
            
        }
    }
}