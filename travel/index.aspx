﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="travel.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <h3>Travel Packages</h3>
    </div>
    <div class="container-fluid">
        <asp:GridView ID="gvTravelPackages" runat="server" DataKeyNames="travel_package_id" AutoGenerateColumns="False" Font-Bold="False" HorizontalAlign="Center" OnRowDeleting="gvTravelPackages_RowDeleting" OnRowCommand="gvTravelPackages_RowCommand" CssClass="table table-striped">
        <Columns>
            <asp:BoundField DataField="travel_package_id" HeaderText="Travel Package ID" />
            <asp:BoundField DataField="package_name" HeaderText="Name" />
            <asp:BoundField DataField="start_date" HeaderText="Start Date" />
            <asp:BoundField DataField="end_date" HeaderText="End Date" />
            <asp:BoundField DataField="min_pax" HeaderText="Min. Pax" />
            <asp:BoundField DataField="min_night" HeaderText="Min. Night" />
            <asp:BoundField DataField="max_night" HeaderText="Max. Night" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Edit" CommandName="EditTravelPackage" CommandArgument='<%#Eval("travel_package_id")%>'></asp:LinkButton>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <HeaderStyle HorizontalAlign="Center" />
        <RowStyle Font-Bold="False" HorizontalAlign="Center" />
    </asp:GridView>
    </div>
</asp:Content>
