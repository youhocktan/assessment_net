﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app.Master" AutoEventWireup="true" CodeBehind="viewRoomPackages.aspx.cs" Inherits="travel.viewRoomPackages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid bottom-pad">
        <h3>Air Packages</h3>
        <div>
            <div class="col-sm-6 zero-padding-left"><asp:button ID="btnCreate" runat="server" Text="Add New Package" OnClick="btnCreate_Click" CssClass="btn btn-default"></asp:button></div>
            <div class="col-sm-6 zero-padding-right"><asp:DropDownList ID="ddlTP" runat="server" CssClass="btn btn-default pull-right" OnSelectedIndexChanged="ddlTP_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
        </div>
    </div>
    <div class="container-fluid">
        <asp:GridView ID="gvRoomPackages" runat="server" DataKeyNames="room_package_id" AutoGenerateColumns="False" Font-Bold="False" HorizontalAlign="Center" OnRowDeleting="gvRoomPackages_RowDeleting" OnRowCommand="gvRoomPackages_RowCommand" CssClass="table table-striped">
        <Columns>
            <asp:BoundField DataField="room_package_id" HeaderText="Air Package ID" />
            <asp:BoundField DataField="package_name" HeaderText="Travel Package" />
            <asp:BoundField DataField="start_date" HeaderText="Start Date" />
            <asp:BoundField DataField="end_date" HeaderText="End Date" />
            <asp:BoundField DataField="room_type" HeaderText="Room Type" />
            <asp:BoundField DataField="bed_type" HeaderText="Bed Type" />
            <asp:BoundField DataField="price_adult" HeaderText="Price/Adult" />
            <asp:BoundField DataField="price_child" HeaderText="Price/Child 4-11" />
            <asp:BoundField DataField="price_toddler" HeaderText="Price/Child 0-3" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Edit" CommandName="EditRoomPackage" CommandArgument='<%#Eval("room_package_id")%>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Get Prices" CommandName="CalculateRoomPackage" CommandArgument='<%#Eval("room_package_id")%>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle HorizontalAlign="Center" />
        <RowStyle Font-Bold="False" HorizontalAlign="Center" />
    </asp:GridView>
    </div>
</asp:Content>
