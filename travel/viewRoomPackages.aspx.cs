﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using travel.lib;

namespace travel
{
    public partial class viewRoomPackages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadTravelPackages();
                LoadRoomPackages();
            }
        }

        private void LoadTravelPackages()
        {
            //call and execute stored procedure
            SqlCommand comm = new SqlCommand("RetrieveAllTravelPackages");
            DataTable packagesTable = DB.SPExecuteSelectStatements(comm);

            DataView view = packagesTable.AsDataView();
            view.Sort = "package_name asc";
            packagesTable = view.ToTable();

            foreach (DataRow packagesRow in packagesTable.Rows)
            {
                //add all items into dropdownlist
                ListItem item = new ListItem(packagesRow["package_name"].ToString(), packagesRow["id"].ToString());
                ddlTP.Items.Add(item);
            }
        }

        private void LoadRoomPackages()
        {
            //init datatable
            DataTable tableToBind = new DataTable();
            tableToBind.Columns.Add("room_package_id");
            tableToBind.Columns.Add("package_name");
            tableToBind.Columns.Add("start_date");
            tableToBind.Columns.Add("end_date");
            tableToBind.Columns.Add("room_type");
            tableToBind.Columns.Add("bed_type");
            tableToBind.Columns.Add("price_adult");
            tableToBind.Columns.Add("price_child");
            tableToBind.Columns.Add("price_toddler");

            SqlCommand comm = new SqlCommand("RetrieveRoomPackagesByTravelPackageId");
            comm.Parameters.AddWithValue("@tp_id", ddlTP.SelectedValue);
            DataTable allRPackages = DB.SPExecuteSelectStatements(comm);

            //get all necessary data and throw to datatable
            foreach (DataRow packagesRow in allRPackages.Rows)
            {
                string id = packagesRow["room_package_id"].ToString();
                int tp_id = Convert.ToInt32(packagesRow["travel_package_id"]);
                string t_start = packagesRow["travel_sdate"].ToString();
                string t_end = packagesRow["travel_edate"].ToString();
                int room_type = Convert.ToInt32(packagesRow["room_type"]);
                int bed_type = Convert.ToInt32(packagesRow["bed_type"]);
                string price_adult = packagesRow["price_adult"].ToString();
                string price_child = packagesRow["price_child"].ToString();
                string price_toddler = packagesRow["price_toddler"].ToString();

                comm = new SqlCommand("RetrieveTravelPackageById");
                comm.Parameters.AddWithValue("@id", tp_id);
                DataTable tp = DB.SPExecuteSelectStatements(comm);
                string tp_name = tp.Rows[0]["package_name"].ToString();

                comm = new SqlCommand("RetrieveRoomTypeById");
                comm.Parameters.AddWithValue("@room_id", room_type);
                DataTable room = DB.SPExecuteSelectStatements(comm);
                string room_name = room.Rows[0]["type"].ToString();

                comm = new SqlCommand("RetrieveBedTypeById");
                comm.Parameters.AddWithValue("@bed_id", bed_type);
                DataTable bed = DB.SPExecuteSelectStatements(comm);
                string bed_name = bed.Rows[0]["type"].ToString();

                //now we are done lets add it to the row of the datatable
                tableToBind.Rows.Add(id, tp_name, t_start, t_end, room_name, bed_name, price_adult, price_child, price_toddler);
            }

            //now after iterating through all users, lets bind it to the gridview
            //hide ID column
            gvRoomPackages.Columns[0].Visible = false;
            gvRoomPackages.DataSource = tableToBind;
            gvRoomPackages.DataBind();
        }

        protected void gvRoomPackages_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvRoomPackages.DataKeys[e.RowIndex].Value.ToString());
            System.Diagnostics.Debug.Print(id + "");

            SqlCommand comm = new SqlCommand("DeleteRoomPackageById");
            comm.Parameters.AddWithValue("@rp_id", id);

            DB.SPExecuteNonQuery(comm);

            LoadRoomPackages();

        }

        protected void gvRoomPackages_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (Session["roomPackageID"] != null)
            {
                Session["roomPackageID"] = e.CommandArgument;
            }
            else
            {
                Session.Add("roomPackageID", e.CommandArgument);
            }

            if (e.CommandName == "EditRoomPackage")
            {
                Response.Redirect("~/EditRoomPackage.aspx");
            }
            else if (e.CommandName == "CalculateRoomPackage")
            {
                Response.Redirect("~/CalcRoomPackage.aspx");
            }


        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CreateRoomPackage.aspx");
        }

        protected void ddlTP_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRoomPackages();
        }
    }
}