﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app.Master" AutoEventWireup="true" CodeBehind="CalcRoomPackage.aspx.cs" Inherits="travel.CalcRoomPackage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><asp:Literal id="travel_package_name" Text="" runat="server"/></h3>
            </div>
            <div class="panel-body">
                <p><strong>Package Date: </strong><asp:Label ID="package_date" runat="server"></asp:Label></p>
                <p><strong>Room Type: </strong><asp:Label ID="r_type" runat="server"></asp:Label></p>
                <p><strong>Bed Type: </strong><asp:Label ID="b_type" runat="server"></asp:Label></p>
                <p><strong>Price per Adult(Package): </strong><asp:Label ID="price_adult_p" runat="server"></asp:Label></p>
                <p><strong>Price per Child 4 - 11 yrs(Package): </strong><asp:Label ID="price_child_p" runat="server"></asp:Label></p>
                <p><strong>Price per Child 2 - 3 yrs(Package): </strong><asp:Label ID="price_toddler_p" runat="server"></asp:Label></p>
                <p><strong>Price per Adult(Extension): </strong><asp:Label ID="price_adult_e" runat="server"></asp:Label></p>
                <p><strong>Price per Child 4 - 11 yrs(Extension): </strong><asp:Label ID="price_child_e" runat="server"></asp:Label></p>
                <p><strong>Price per Child 0 - 3 yrs(Extension): </strong><asp:Label ID="price_toddler_e" runat="server"></asp:Label></p>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div role="form">
            <div class="form-group">
                <label for="travel_start">Travel Start Date</label>
                <asp:TextBox ID="travel_start" runat="server" type="date" class="form-control" placeholder="Start Date"  required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="travel_end">Travel End Date</label>
                <asp:TextBox ID="travel_end" runat="server" type="date" class="form-control" placeholder="End Date" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="numAdult">Number of Adult(s)</label>
                <asp:TextBox ID="numAdult" runat="server" type="number" step="any" min="2" class="form-control" placeholder="Number of Adult" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="numChild">Number of Children 4 - 11 yrs</label>
                <asp:TextBox  ID=numChild runat="server" type="number" step="any" min="0" class="form-control" placeholder="Number of Child 4 - 11 yrs" required></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="numToddler">Number of Children 0 - 3 yrs</label>
                <asp:TextBox  ID=numToddler runat="server" type="number" step="any" min="0" class="form-control" placeholder="Number of Child 0 - 3 yrs" required></asp:TextBox>
            </div>
    </div>
    <div>
        <asp:Button ID="btnCalc" runat="server" Text="Calculate Amount" OnClick="btnCalc_Click" ValidationGroup="create" CssClass="btn btn-default" />
        <p class="bg-danger" runat="server" id="error" visible="false">Error.</p>
    </div>
    <div class="container-fluid" id="breakdown_amount" runat="server"></div>
    </div>
</asp:Content>
